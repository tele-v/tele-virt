#!/usr/bin/env python3.8
#
# Author:   jon4h
# Date:     20.11.2019
# Desc:     Telegram BOT for Tele-V Project
# Version:  1.0
###################################################################################################

# import packages/modules
from telegram.ext import Updater, InlineQueryHandler, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, ConversationHandler
from telegram import KeyboardButton, ReplyKeyboardMarkup, ChatAction, InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
import subprocess, json


#==================================================================================================
# VARIABLES
#==================================================================================================

# read telegram token from file
with open('token.txt') as f:
    TOKEN = f.read()

# "root" path of the script
path = 'D:\\Scripts\\Tele-V\\VMMgmt'

# location of the json file
json_file = '%s\\VMs.json' % path

# emojis in dictionary
emoji = {
    'shit':'\U0001F4A9',
    'accept':'\U00002705',
    'deny':'\U0000274C',
    'create':'\U0001F916',
    'manage':'\U0001F579'
}

# empty dictionarys for variable assignment per userid
selected_vm_name = {}
selected_vm_cpu = {}
selected_vm_ram = {}
selected_vm_disk = {}
selected_vm_os = {}

show_vm_name = {}
show_vm_cpu = {}
show_vm_ram = {}
show_vm_disk = {}
show_vm_os = {}
show_vm_status = {}

vms = {}

# reply markup keyboard buttons (with emojis)
button_command = {
    'create_vm':'%s CREATE' % emoji['create'], 
    'manage_vm':'%s MANAGE' % emoji['manage']
}

# shortcut
END = ConversationHandler.END

# declare variables for conversation states
(
    STOPPING, MANAGE_VM, SELECT_ACTION, SHOW, BOOT, SHUTDOWN, 
    DELETE, CONFIRM_SHUTDOWN, CONFIRM_DELETE, ACTION_SHUTDOWN, 
    ACTION_DELETE, DENY_ACTION, CREATE_VM, CONFIRM_VM_NAME, 
    CONFIRM_NAME, SELECT_VM_CPU, GO_BACK_TO_SELECT, MANAGE_GO_BACK, 
    DENY_NAME, RESELECT_NAME, SELECT_VM_RAM, GO_BACK_TO_CPU, 
    SELECT_VM_DISK, GO_BACK_TO_RAM, SELECT_VM_OS, GO_BACK_TO_DISK, 
    GO_BACK_TO_OS, CONFIRM_VM_CONF, DENY_VM_CONF, CONFIRM_DENY_VM_CONF
)  =  range(30)




#==================================================================================================
# FUNCTIONS
#==================================================================================================

# Load json file
def load_json():
    with open(json_file) as f:
        json_data = json.load(f)
    return json_data

# check if user has permission to use this bot. Read data from json file
def check_permission(update, context, id):
    
    allowed_users = []

    for user in load_json():
        allowed_users.append(int(user['ID']))

    if id in allowed_users:
        return True
    
    else:
        context.bot.send_chat_action(
            chat_id = update.effective_chat.id, 
            action = ChatAction.TYPING
        )
        context.bot.send_message(
            chat_id = update.effective_chat.id, 
            text = 'Sorry, you are not allowed to use this bot. Please leave!'
        )
        context.bot.send_message(
            chat_id = update.effective_chat.id, 
            text = emoji['shit'
        ])

# return reply markup keyboard (custom keyboard)
def reply_keyboard_markup(keyboard_set):
    if keyboard_set == 'start':
        return ReplyKeyboardMarkup([[
            KeyboardButton(button_command['create_vm']),
            KeyboardButton(button_command['manage_vm'])]],
                resize_keyboard=True,
                one_time_keyboard=False)

# Load Vms for user
def get_vms(id):
    for i in load_json():
        if int(i['ID']) == id:
            return i['VMs']

# check if vm name is available
def check_vm_name(vmname):
    for i in load_json():
        try:
            if vmname in i['VMs']:
                return True
        except TypeError:
            pass

# Generate inline reply buttons
def reply_inlinekeyboard_markup(keyboard_set, id=0):
    if keyboard_set == 'buttons_select_vm':
        buttons = []

        for vm in vms[id]:
            buttons.append([InlineKeyboardButton(
                vm, callback_data='vm_'+vm)])
            
        keyboard = InlineKeyboardMarkup(buttons)
        
    elif keyboard_set == 'buttons_select_vm_name_confirm_deny':
        buttons = [[
                InlineKeyboardButton(text='%s CONFIRM NAME' % emoji['accept'], callback_data=str(CONFIRM_NAME))
            ],[
                InlineKeyboardButton(text='%s DENY NAME' % emoji['deny'], callback_data=str(DENY_NAME))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)

    elif keyboard_set == 'buttons_select_action':
        buttons = [[
                InlineKeyboardButton(text='SHOW', callback_data=str(SHOW)),
                InlineKeyboardButton(text='BOOT', callback_data=str(BOOT))
            ],[
                InlineKeyboardButton(text='SHUTDOWN', callback_data=str(SHUTDOWN)),
                InlineKeyboardButton(text='DELETE', callback_data=str(DELETE)),
            ],[
                InlineKeyboardButton(text='<< GO BACK', callback_data=str(GO_BACK_TO_SELECT))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)

    elif keyboard_set == 'buttons_select_action_shutdown':
        buttons = [[
                InlineKeyboardButton(text='%s CONFIRM SHUTDOWN' % emoji['accept'], callback_data=str(CONFIRM_SHUTDOWN))
            ],[
                InlineKeyboardButton(text='%s DENY SHUTDOWN' % emoji['deny'], callback_data=str(DENY_ACTION))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)
    
    elif keyboard_set == 'buttons_select_action_delete':
        buttons = [[
                InlineKeyboardButton(text='%s CONFIRM DELETE' % emoji['accept'], callback_data=str(CONFIRM_DELETE))
            ],[
                InlineKeyboardButton(text='%s DENY DELETE' % emoji['deny'], callback_data=str(DENY_ACTION))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)

    elif keyboard_set == 'buttons_select_cpu':
        buttons = [[
                InlineKeyboardButton(text='1', callback_data='cpu_1')
            ],[
                InlineKeyboardButton(text='2', callback_data='cpu_2')
        ]]
        keyboard = InlineKeyboardMarkup(buttons)

    elif keyboard_set == 'buttons_select_ram':
        buttons = [[
            InlineKeyboardButton(text='1 GB', callback_data='ram_1'),
                InlineKeyboardButton(text='2 GB', callback_data='ram_2')
            ],[
                InlineKeyboardButton(text='4 GB', callback_data='ram_4'),
                InlineKeyboardButton(text='8 GB', callback_data='ram_8')
            ],[
                InlineKeyboardButton(text='<< Back', callback_data=str(GO_BACK_TO_CPU))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)        
    
    elif keyboard_set == 'buttons_select_disk':
        buttons = [[
                InlineKeyboardButton(text='10 GB', callback_data='disk_10'),
                InlineKeyboardButton(text='20 GB', callback_data='disk_20')
            ],[
                InlineKeyboardButton(text='40 GB', callback_data='disk_40'),
                InlineKeyboardButton(text='80 GB', callback_data='disk_80')
            ],[
                InlineKeyboardButton(text='<< Back', callback_data=str(GO_BACK_TO_RAM))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)

    elif keyboard_set == 'buttons_select_os':
        buttons = [[
                InlineKeyboardButton(text='Zorin OS', callback_data='os_Zorin'),
                InlineKeyboardButton(text='Windows OS', callback_data='os_Windows')
            ],[
                InlineKeyboardButton(text='<< Back', callback_data=str(GO_BACK_TO_DISK))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)
    
    elif keyboard_set == 'buttons_confirm_vm_conf':
        buttons = [[
                InlineKeyboardButton(text='%s CONFIRM SETTINGS' % emoji['accept'], callback_data=str(CONFIRM_VM_CONF)),
            ],[
                InlineKeyboardButton(text='%s RECONFIGURE SETTINGS' % emoji['deny'], callback_data=str(DENY_VM_CONF))
            ],[
                InlineKeyboardButton(text='<< Back', callback_data=str(GO_BACK_TO_OS))
        ]]
        keyboard = InlineKeyboardMarkup(buttons)


    return keyboard



#==================================================================================================
# MESSAGE HANDLERS
#==================================================================================================

# Top level conversation, handle manage and create button
def conv_handler_func(update, context):
    if check_permission(update, context, update.effective_chat.id):
        if update.message.text == button_command['manage_vm']:
            
            vms[update.effective_chat.id] = get_vms(update.effective_chat.id)
            if not vms[update.effective_chat.id]:
                text = 'Sorry you have no VMs. Please create a VM first.'
                
                context.bot.send_chat_action(
                    chat_id = update.effective_chat.id, 
                    action = ChatAction.TYPING
                )
                context.bot.send_message(
                    chat_id = update.effective_chat.id, 
                    text = text)
                

                return END
            
            else:
                text = 'Select a VM'           
                
                context.bot.send_message(
                    chat_id = update.effective_chat.id, 
                    text = text, 
                    reply_markup = reply_inlinekeyboard_markup('buttons_select_vm', update.effective_chat.id)
                )

                return MANAGE_VM
        
        elif update.message.text == button_command['create_vm']:
            if check_permission(update, context, update.effective_chat.id):
                text = 'Enter Name for VM:'
                context.bot.send_message(
                    chat_id = update.effective_chat.id, 
                    text = text
                )
                
                return CREATE_VM


# 2. level conv, select vm name (create)
def select_vm_name(update, context):
    if check_permission(update, context, update.effective_chat.id):       
        selected_vm_name[update.effective_chat.id] = update.message.text

        if check_vm_name(selected_vm_name[update.effective_chat.id]):
            context.bot.send_message(
                chat_id = update.effective_chat.id,
                text = 'Sorry this VM Name is not available. Please select an other one or cancel the action with /cancel'
            )
            return RESELECT_NAME
        
        else:
            context.bot.send_message(
                chat_id = update.effective_chat.id,
                text = 'VM Name: %s' % selected_vm_name[update.effective_chat.id], 
                reply_markup = reply_inlinekeyboard_markup('buttons_select_vm_name_confirm_deny')
            )
            return CONFIRM_VM_NAME

#==================================================================================================
# CALLBACKQUERY HANDLERS, 2. level
#==================================================================================================

# return actions after select vm (manage)
def manage_vm_selected_vm(update, context):        
    selected_vm_name[update.effective_chat.id] = update.callback_query.data[3:]
    text = 'Selected VM: %s' % selected_vm_name[update.effective_chat.id]
    
    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_action', 0)
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return SELECT_ACTION

# show vm infos
def manage_vm_show(update, context):
    scriptpath = '%s\\Get-Info.ps1' % path

    output = subprocess.check_output(["powershell.exe", "Set-Location %s;%s -VMName %s -TID %s " %(
        path, 
        scriptpath, 
        selected_vm_name[update.effective_chat.id], 
        update.effective_chat.id
    )])

    output = str(output)[2:len(str(output))-5]

    json_data_show = json.loads(output)

    show_vm_name[update.effective_chat.id] = json_data_show['Name']
    show_vm_cpu[update.effective_chat.id] = json_data_show['Processor']
    show_vm_ram[update.effective_chat.id] = json_data_show['RAM']
    show_vm_disk[update.effective_chat.id] = json_data_show['DiskSpace']
    show_vm_os[update.effective_chat.id] = json_data_show['OS']
    show_vm_status[update.effective_chat.id] = json_data_show['State']
    text = 'VM NAME: %s\nCPU: %s\nRAM: %s\nDisk Space: %s\nOS: %s\nStatus: %s' % (
        show_vm_name[update.effective_chat.id],
        show_vm_cpu[update.effective_chat.id],
        show_vm_ram[update.effective_chat.id],
        show_vm_disk[update.effective_chat.id],
        show_vm_os[update.effective_chat.id],
        show_vm_status[update.effective_chat.id]
    )
    
    context.bot.answer_callback_query(
        update.callback_query.id,
        show_alert = True, 
        text = text
    )

    return SELECT_ACTION

# boot vm (manage)
def manage_vm_boot(update, context):
    text = 'Booting VM: %s' % selected_vm_name[update.effective_chat.id]

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text
    )


    file_path = '%s\\Start-VM.ps1' % path

    output = subprocess.check_output(
        ["powershell.exe", 
        "Set-Location %s;%s -VMName %s -TID %s" % (
            path, 
            file_path, 
            selected_vm_name[update.effective_chat.id], 
            update.effective_chat.id 
        )]
    )


    output = str(output)[2:len(str(output))-5]

    if output == 'True':
        text = 'Your VM: "%s" started successfully!' % selected_vm_name[update.effective_chat.id]
    else:
        text = '<b>Error</b>: an error has occurred while starting the VM. Please retry or contact an administrator. https://t.me/tele_v_support'
    
    context.bot.send_chat_action(
        chat_id = update.effective_chat.id, 
        action = ChatAction.TYPING
    )
    context.bot.send_message(
        chat_id = update.effective_chat.id, 
        text = text,
        parse_mode=ParseMode.HTML
    )

    return END

# shut vm down
def manage_vm_shutdown(update, context):
    text = 'Please Confirm Shutdown...'
    
    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_action_shutdown')
    )

    return ACTION_SHUTDOWN

# delete vm, must be confirmed
def manage_vm_delete(update, context): 
    text = 'Please Confirm Delete...'

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_action_delete')
    )

    return ACTION_DELETE

# confirm shutdown for selected vm
def confirm_shutdown(update, context):
    text='Shuting down VM: %s' % selected_vm_name[update.effective_chat.id]

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text
    )


    file_path = '%s\\Stop-VM.ps1' % path

    output = subprocess.check_output(
        ["powershell.exe", 
        "Set-Location %s;%s -VMName %s -TID %s" % (
            path, 
            file_path, 
            selected_vm_name[update.effective_chat.id], 
            update.effective_chat.id
        )]
    )


    output = str(output)[2:len(str(output))-5]

    if output == 'True':
        text = 'Your VM: "%s" shut down successfully!' % selected_vm_name[update.effective_chat.id]

    else:
        text = '<b>Error</b>: an error has occurred while shuting down the VM. Please retry or contact an administrator. https://t.me/tele_v_support'
    context.bot.send_chat_action(
        chat_id = update.effective_chat.id, 
        action = ChatAction.TYPING
    )
    context.bot.send_message(
        chat_id = update.effective_chat.id, 
        text = text,
        parse_mode=ParseMode.HTML
    )

    
    return END

# confirm delete for selected vm
def confirm_delete(update, context):
    text='Deleting VM: %s' % selected_vm_name[update.effective_chat.id]

    query = update.callback_query
    bot = context.bot

    context.bot.send_chat_action(
        chat_id = update.effective_chat.id, 
        action = ChatAction.TYPING
    )
    bot.edit_message_text(
        chat_id = update.effective_chat.id,
        message_id = query.message.message_id,
        text = text
    )

    file_path = '%s\\Remove-VM.ps1' % path

    output = subprocess.check_output(["powershell.exe", "Set-Location %s;%s -VMName %s -TID %s " %(
        path, 
        file_path, 
        selected_vm_name[update.effective_chat.id], 
        update.effective_chat.id,
    )])

    output = str(output)[2:len(str(output))-5]

    if output == 'True':
        text = 'The VM: %s was deleted successfully!' % selected_vm_name[update.effective_chat.id]
    else:
        text = '<b>Error</b>: an error has occurred while deleting the VM. Please retry or contact an administrator. https://t.me/tele_v_support'
    
    context.bot.send_chat_action(
        chat_id = update.effective_chat.id, 
        action = ChatAction.TYPING
    )
    context.bot.send_message(
        chat_id = update.effective_chat.id, 
        text = text,
        parse_mode=ParseMode.HTML
    ) 

    return END

# deny action
def deny_action(update, context):
    text='Denied!'

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text
    )

    return END

# go back button to select an other vm
def go_back_to_select(update, context):
    text = 'Select a VM'
    
    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_vm', query.message.chat_id)
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return MANAGE_GO_BACK

# confirm vm name (create_conv)
def confirm_vm_name(update, context):
    text='VM: %s\nSelect CPUs:' % selected_vm_name[update.effective_chat.id]

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_cpu')
    )
    
    return SELECT_VM_CPU

# deny vm name, reselect or cancel after
def deny_vm_name(update, context):
    text = 'Please select an other name or cancel the action with /cancel'

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text
    )

    return RESELECT_NAME

# select cpu for vm
def create_vm_cpu(update, context):

    if update.callback_query.data[4:] == '1':
        selected_vm_cpu[update.effective_chat.id] = update.callback_query.data[4:]
    elif update.callback_query.data[4:] == '2':
        selected_vm_cpu[update.effective_chat.id] = update.callback_query.data[4:]
    
    text = 'VM: %s\nSelected CPU: %s\n Please select RAM:' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_ram')
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)
    
    return SELECT_VM_RAM

# go back to reselect cpu
def create_go_back_cpu(update, context):
    text = 'VM: %s\nSelect CPUs:' % selected_vm_name[update.effective_chat.id]

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_cpu')
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return SELECT_VM_CPU

# select ram
def create_vm_ram(update, context):
    
    if update.callback_query.data[4:] == '1':
        selected_vm_ram[update.effective_chat.id] = update.callback_query.data[4:]
    elif update.callback_query.data[4:] == '2':
        selected_vm_ram[update.effective_chat.id] = update.callback_query.data[4:]
    elif update.callback_query.data[4:] == '4':
        selected_vm_ram[update.effective_chat.id] = update.callback_query.data[4:]
    elif update.callback_query.data[4:] == '8':
        selected_vm_ram[update.effective_chat.id] = update.callback_query.data[4:]
    

    text = 'VM: %s\nCPU: %s\nRAM: %sGB\nSelect Disk Space:' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id], 
        selected_vm_ram[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_disk')
    )
    
    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)
    
    return SELECT_VM_DISK

# go back to reselect ram
def create_go_back_ram(update, context):
    text = 'VM: %s\nSelected CPU: %s\n Please select RAM:' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id=query.message.message_id,
        text=text, 
        reply_markup=reply_inlinekeyboard_markup('buttons_select_ram')
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return SELECT_VM_RAM

# select disk space
def create_vm_disk(update, context):

    if update.callback_query.data[5:] == '10':
        selected_vm_disk[update.effective_chat.id] = update.callback_query.data[5:]
    elif update.callback_query.data[5:] == '20':
        selected_vm_disk[update.effective_chat.id] = update.callback_query.data[5:]
    elif update.callback_query.data[5:] == '40':
        selected_vm_disk[update.effective_chat.id] = update.callback_query.data[5:]
    elif update.callback_query.data[5:] == '80':
        selected_vm_disk[update.effective_chat.id] = update.callback_query.data[5:]
    
    text = 'VM: %s\nCPU: %s\nRAM: %sGB\nDisk Space: %sGB\nSelect OS:' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id], 
        selected_vm_ram[update.effective_chat.id], 
        selected_vm_disk[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_os')
    )
    
    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return SELECT_VM_OS


# go back to reselect disk
def create_go_back_disk(update, context):
    
    text = 'VM: %s\nCPU: %s\nRAM: %sGB\nSelect Disk Space:' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id], 
        selected_vm_ram[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_disk')
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return SELECT_VM_DISK
    

# select os
def create_vm_os(update, context):

    if update.callback_query.data[3:] == 'Zorin':
        selected_vm_os[update.effective_chat.id] = update.callback_query.data[3:]
    elif update.callback_query.data[3:] == 'Windows':
        selected_vm_os[update.effective_chat.id] = update.callback_query.data[3:]

    text = 'VM: %s\nCPU: %s\nRAM: %sGB\nDisk Space: %sGB\nOS: %s' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id], 
        selected_vm_ram[update.effective_chat.id], 
        selected_vm_disk[update.effective_chat.id], 
        selected_vm_os[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text,
        reply_markup = reply_inlinekeyboard_markup('buttons_confirm_vm_conf')
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return CONFIRM_DENY_VM_CONF

# go back to reselect os
def go_back_to_os(update, context):
    text = 'VM: %s\nCPU: %s\nRAM: %s GB\nDisk Space: %s GB\nSelect OS:' % (
        selected_vm_name[update.effective_chat.id], 
        selected_vm_cpu[update.effective_chat.id], 
        selected_vm_ram[update.effective_chat.id], 
        selected_vm_disk[update.effective_chat.id]
    )

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_os')
    )
    
    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)

    return SELECT_VM_OS

# confirm selected options
def confirm_vm_conf(update, context):
    text='VM is being created, please wait!'

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text
    )

    file_path = '%s\\Create-VM.ps1' % path

    output = subprocess.check_output(
        ["powershell.exe", 
        "Set-Location %s;%s -VMName %s -TID %s -CPU %s -Memory %sGB -VHDSize %sGB -ISO %s" % (
            path, 
            file_path, 
            selected_vm_name[update.effective_chat.id], 
            update.effective_chat.id,
            selected_vm_cpu[update.effective_chat.id], 
            selected_vm_ram[update.effective_chat.id],
            selected_vm_disk[update.effective_chat.id],
            selected_vm_os[update.effective_chat.id] 
        )]
    )


    output = str(output)[2:len(str(output))-5]

    if output == 'True':
        text = '<b>Your VM:</b>\nName: %s\nCPU: %s\nRAM: %sGB\nDISK: %s\nOS: %s' % (
            selected_vm_name[update.effective_chat.id], 
            selected_vm_cpu[update.effective_chat.id], 
            selected_vm_ram[update.effective_chat.id], 
            selected_vm_disk[update.effective_chat.id], 
            selected_vm_os[update.effective_chat.id
        ])
    else:
        text = '<b>Error</b>: an error has occurred while creating the VM. Please retry or contact an administrator. https://t.me/tele_v_support'
    
    context.bot.send_chat_action(
        chat_id = update.effective_chat.id, 
        action = ChatAction.TYPING
    )
    context.bot.send_message(
        chat_id = update.effective_chat.id, 
        text = text,
        parse_mode=ParseMode.HTML
    )


    return END

# deny selected options, reselect or cancel after
def deny_vm_conf(update, context):
    text='VM: %s\nSelect CPUs:' % selected_vm_name[update.effective_chat.id]

    query = update.callback_query
    bot = context.bot

    bot.edit_message_text(
        chat_id = query.message.chat_id,
        message_id = query.message.message_id,
        text = text, 
        reply_markup = reply_inlinekeyboard_markup('buttons_select_cpu')
    )

    # Workaround for Android Client Bug: https://github.com/python-telegram-bot/python-telegram-bot/issues/428
    bot.answer_callback_query(update.callback_query.id)
    
    return SELECT_VM_CPU



#==================================================================================================
# COMMAND HANDLERS
#==================================================================================================

# start commmand
def start(update, context):
    if check_permission(update, context, update.effective_chat.id):

        name = update.message.from_user.first_name

        context.bot.send_chat_action(
            chat_id = update.effective_chat.id, 
            action = ChatAction.TYPING
        )
        context.bot.send_message(
            chat_id = update.effective_chat.id, 
            text = '<b>Welcome %s!</b> \nFor safety reasons we recommend to configure 2 Factor Authentication for Telegram. More information click <a href="https://telegram.org/blog/sessions-and-2-step-verification">Here</a>' % name, 
            reply_markup = reply_keyboard_markup('start'),
            parse_mode = ParseMode.HTML
        )


# cancel command
def cancel(update, context):
    if check_permission(update, context, update.effective_chat.id):
        text = 'Bye!'

        context.bot.send_chat_action(
            chat_id = update.effective_chat.id,
            action = ChatAction.TYPING
        )
        context.bot.send_message(
            chat_id = update.effective_chat.id,
            text = text
        )
                
        return END



#==================================================================================================
# ERROR HANDLER
#==================================================================================================

# return errors to user
def error(update, context):
    context.bot.send_message(
        chat_id = update.effective_chat.id, 
        text = 'Error: %s' % context.error
    )
    return END

#==================================================================================================
# MAIN
#==================================================================================================

# main function
def main():
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher
    
    #============================================
    # Second level, manage_conv handler
    #============================================

    manage_vm_conv = ConversationHandler(

        # entry point to trigger listener
        entry_points = [CallbackQueryHandler(manage_vm_selected_vm, pattern='^vm',)],

        # different states of conversation
        states = {
            SELECT_ACTION: [
                CallbackQueryHandler(manage_vm_show, pattern='^' + str(SHOW) + '$'),
                CallbackQueryHandler(manage_vm_boot, pattern='^' + str(BOOT) + '$'),
                CallbackQueryHandler(manage_vm_shutdown, pattern='^' + str(SHUTDOWN) + '$'),
                CallbackQueryHandler(manage_vm_delete, pattern='^' + str(DELETE) + '$'),
                CallbackQueryHandler(go_back_to_select, pattern='^' + str(GO_BACK_TO_SELECT) + '$')
            ],
            ACTION_SHUTDOWN: [
                CallbackQueryHandler(confirm_shutdown, pattern='^' + str(CONFIRM_SHUTDOWN) + '$'),
                CallbackQueryHandler(deny_action, pattern='^' + str(DENY_ACTION) + '$')
            ],
            ACTION_DELETE: [
                CallbackQueryHandler(confirm_delete, pattern='^' + str(CONFIRM_DELETE) + '$'),
                CallbackQueryHandler(deny_action, pattern='^' + str(DENY_ACTION) + '$')
            ],
        },

        # fallback to cancel commands, works at any state of conversation
        fallbacks = [CommandHandler('cancel', cancel)],

        # go back to top level conversation
        map_to_parent = {
            END: STOPPING,
            MANAGE_GO_BACK: MANAGE_VM
        }
    )
    #============================================
    # Second level, create_conv handler
    #============================================

    create_vm_conv = ConversationHandler(

        # entry point to trigger listener
        entry_points = [MessageHandler(Filters.text, select_vm_name)],
        
        # different states of conversation
        states = {
            CONFIRM_VM_NAME: [
                CallbackQueryHandler(confirm_vm_name, pattern='^' + str(CONFIRM_NAME) + '$'),
                CallbackQueryHandler(deny_vm_name, pattern='^' + str(DENY_NAME) + '$')
            ],
            SELECT_VM_CPU: [
                CallbackQueryHandler(create_vm_cpu, pattern='^cpu')
            ],
            SELECT_VM_RAM: [
                CallbackQueryHandler(create_vm_ram, pattern='^ram'),
                CallbackQueryHandler(create_go_back_cpu, pattern='^' + str(GO_BACK_TO_CPU) + '$')
            ],
            SELECT_VM_DISK: [
                CallbackQueryHandler(create_vm_disk, pattern='^disk'),
                CallbackQueryHandler(create_go_back_ram, pattern='^' + str(GO_BACK_TO_RAM) + '$')
            ],
            SELECT_VM_OS: [
                CallbackQueryHandler(create_vm_os, pattern='^os'),
                CallbackQueryHandler(create_go_back_disk, pattern='^' + str(GO_BACK_TO_DISK) + '$')
            ],
            CONFIRM_DENY_VM_CONF: [
                CallbackQueryHandler(confirm_vm_conf, pattern='^' + str(CONFIRM_VM_CONF) + '$'),
                CallbackQueryHandler(deny_vm_conf, pattern='^' + str(DENY_VM_CONF) + '$'),
                CallbackQueryHandler(go_back_to_os, pattern='^' + str(GO_BACK_TO_OS) + '$')
            ]
        },

        # fallback to cancel commands, works at any state of conversation
        fallbacks = [CommandHandler('cancel', cancel)],

        # go back to top level conversation
        map_to_parent = {
            END: STOPPING,
            RESELECT_NAME: CREATE_VM
        }
    )

    #============================================
    # Top level, conv handler
    #============================================

    conv_handler = ConversationHandler(

        # entry point to trigger listener
        entry_points = [MessageHandler(
            Filters.regex('^(%s|%s)$' % (
                button_command['create_vm'], 
                button_command['manage_vm']
            )), 
            conv_handler_func
        )],
        
         # different states of conversation
        states = {
            MANAGE_VM: [manage_vm_conv],
            CREATE_VM: [create_vm_conv]
        },

        # fallback to cancel commands, works at any state of conversation
        fallbacks = [CommandHandler('cancel', cancel)]
    )
    

    # command handlers
    dp.add_handler(CommandHandler('start',start))
    dp.add_handler(conv_handler)

    dp.add_error_handler(error)

    # trigger special listener to go back to the entrypoints
    conv_handler.states[STOPPING] = conv_handler.entry_points
    conv_handler.states[MANAGE_GO_BACK] = manage_vm_conv.entry_points
    conv_handler.states[RESELECT_NAME] = create_vm_conv.entry_points
    
    # start bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
